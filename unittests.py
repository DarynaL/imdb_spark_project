import unittest
import data_processor
import spark
dp = data_processor.dataProcessor()


class TestMethods(unittest.TestCase):
    """Class containing test methods for every method of dataProcessor class.
    Child class for TestCase class
    Contains list with names of tables
    """
    tlist = ["title.ratings",  # 0
             "title.episode",  # 1
             "title.principals",  # 2
             "title.crew",  # 3
             "title.basics",  # 4
             "title.akas",  # 5
             "name.basics"]  # 6

    def test_getDF(self):
        """Testing method for method getDataFrame()
        Checks whether number of lines in each table still equals one of numbers in list
        Returns:
            None
        """
        llist = [1113435, 5445877, 42606026, 7523302, 7523302, 24863539, 10648091]
        i = 0
        for table in self.tlist:
            l = dp.getDataFrame(table).count()
            self.assertEqual(l, llist[i])
            i += 1

    def test_getUL(self):
        """Testing method for method getUkrLanguage()
        Checks whether number of lines in selected table still equals detected number
        Returns:
            None
        """
        l = 17806
        self.assertEqual(dp.getUkrLanguage().count(), l)

    def test_getB19th(self):
        """Testing method for method getBorn19th()
        Checks whether number of lines in selected table still equals detected number
        Returns:
            None
        """
        l = 39646
        self.assertEqual(dp.getBorn19th().count(), l)

    def test_getM2h(self):
        """Testing method for method getMovies2h()
        Checks whether number of lines in selected table still equals detected number
        Returns:
            None
        """
        l = 34136
        self.assertEqual(dp.getMovies2h().count(), l)

    def test_getActors(self):
        """Testing method for method getActors()
        Checks whether number of lines in selected table still equals detected number
        Returns:
            None
        """
        l = 21459239
        self.assertEqual(dp.getActors().count(), l)

    def test_getAdultMPerR(self):
        """Testing method for method getAdultMoviesPerRegion()
        Checks whether number of lines in selected table still equals detected number
        Also checks whether first three values in selected table still equal detected values
        Returns:
            None
        """
        l = 96
        df = dp.getAdultMoviesPerRegion()
        self.assertEqual(df.count(), l)
        self.assertEqual(df.collect()[0][0], "US")
        self.assertEqual(df.collect()[1][0], "JP")
        self.assertEqual(df.collect()[2][0], "DE")
        self.assertEqual(df.collect()[0][1], 85091)
        self.assertEqual(df.collect()[1][1], 19755)
        self.assertEqual(df.collect()[2][1], 8639)

    def test_getEpisodes(self):
        """Testing method for method getEpisodes()
        Checks whether number of lines in selected table still equals detected number
        Also checks whether fourth value in selected table still equals detected value
        Returns:
            None
        """
        l = 50
        df = dp.getEpisodes()
        self.assertEqual(df.count(), l)
        self.assertEqual(df.collect()[4][0], "Charlie Rose")
        self.assertEqual(df.collect()[4][1], 9857)

    def test_getPopSeriesByD(self):
        """Testing method for method getPopularMoviesByD()
        Checks whether number of lines in selected table still equals detected number
        Checks whether minimal year in selected table still equals detected minimal year
        Returns:
            None
        """
        l = 153
        df = dp.getPopularSeriesByD()
        self.assertEqual(df.count(), l)
        df.createOrReplaceTempView("table")
        query = dp.spark.sql("SELECT MIN(Year) FROM table").collect()[0][0]
        self.assertGreaterEqual(query, 1874)

    def test_getPopSeriesByG(self):
        """Testing method for method getPopularSeriesByG()
        Checks whether number of lines in selected table still equals detected number
        Returns:
            None
        """
        l = 271
        df = dp.getPopularSeriesByG()
        self.assertEqual(dp.getPopularSeriesByG().count(), l)


if __name__ == '__main__':
    unittest.main()

# IMDb Spark Project
Develop ETL pipeline to process [IMDb](https://www.imdb.com/interfaces/) dataset using Apache Spark and Python language.

### Extraction

- Create appropriate schemas for all datasets is use.
- Create DataFrames reading data from the remote storage according to corresponding schemas 

### Transformation

- Get all titles of piece of art that are available in Ukrainian.
- Get the list of people’s names, who were born in the 19th century.
- Get titles of all movies that last more than 2 hours.
- Get names of people, corresponding movies/series and characters they played in those films.
- Get the top 100 regions with highest relation "adult movies to regular movies".
- Get the top 50 TV Series with the biggest quantity of episodes.
- Get 10 titles of the most popular product of art by each decade.
- Get 10 titles of the most popular product of art by each genre.

### Loading

Load all results of transformations to .csv files.

### Implementation

The solution contains next classes:

- dataProcessor - has methods to load data from remote storage, do data transformations and save the results to csv files
- unittests - tests dataProcessor functionality.

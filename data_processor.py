from pyspark import SparkContext
from pyspark import SparkFiles
from pyspark.sql import SparkSession
from pyspark.sql.types import *
from pyspark.sql.functions import UserDefinedFunction
sc = SparkContext()
sc.setLogLevel("OFF")



class dataProcessor:
    """Class does a whole task for data processing
        It has separate methods for every task and one common method  for reading tables
    """
    """Connecting to SparkSession"""
    spark = SparkSession.builder \
        .master("local") \
        .appName("data_processor") \
        .getOrCreate()

    def getDataFrame(self, table_name):
        """ Method is public for testing purposes
        Contains list with names of tables on a remote storage and schemas for every table
        Method gets name of a table, downloads it from a remote storage and
        loads to a Dataframe according to provided schema.
        Args:
            table_name(str): name of a table dataframe of which you want to get
        Returns:
            PySpark Dataframe with a full table
        """
        prefix = "https://datasets.imdbws.com/"
        flist = [prefix+"title.ratings.tsv.gz",
                 prefix+"title.episode.tsv.gz",
                 prefix+"title.principals.tsv.gz",
                 prefix+"title.crew.tsv.gz",
                 prefix+"title.basics.tsv.gz",
                 prefix+"title.akas.tsv.gz",
                 prefix+"name.basics.tsv.gz"]
        i = 0
        for i in range(len(flist)):
            if flist[i].find(table_name) > -1:
                break
        slist = []
        """Schemas for all tables"""
        s_rating = StructType([StructField("title_id", StringType(), True),
                              StructField("rating", DecimalType(), False),
                              StructField("votes", IntegerType(), False)
                            ])
        slist.append(s_rating)
        s_episode = StructType([StructField("episode_id", StringType(), True),
                              StructField("series_id", StringType(), True),
                              StructField("season_num", ShortType(), False),
                              StructField("episode_num", ShortType(), False)
                            ])
        slist.append(s_episode)
        s_principals = StructType([StructField("title_id", StringType(), True),
                              StructField("ordering", ByteType(), True),
                              StructField("name_id", StringType(), True),
                              StructField("category", StringType(), True),
                              StructField("job", StringType(), False),
                              StructField("characters", StringType(), False)
                            ])
        slist.append(s_principals)
        s_crew = StructType([StructField("title_id", StringType(), True),
                              StructField("directors_ids", StringType(), True),
                              StructField("writers_ids", StringType(), False)
                            ])
        slist.append(s_crew)
        s_basics = StructType([StructField("title_id", StringType(), True),
                              StructField("title_type", StringType(), True),
                              StructField("primary_title", StringType(), True),
                              StructField("original_title", StringType(), True),
                              StructField("is_adult", ByteType(), True),
                              StructField("start_year", ShortType(), True),
                              StructField("end_year", ShortType(), False),
                              StructField("runtime_minutes", ShortType(), False),
                              StructField("genres", StringType(), False)
                            ])
        slist.append(s_basics)
        s_akas = StructType([StructField("title_id", StringType(), True),
                              StructField("ordering", ByteType(), True),
                              StructField("title", StringType(), True),
                              StructField("region", StringType(), False),
                              StructField("language", StringType(), False),
                              StructField("types", StringType(), False),
                              StructField("attributes", StringType(), False),
                              StructField("is_original_title", BooleanType(), True)
                            ])
        slist.append(s_akas)
        s_names = StructType([StructField("name_id", StringType(), True),
                              StructField("name", StringType(), True),
                              StructField("birth_year", ShortType(), True),
                              StructField("death_year", ShortType(), False),
                              StructField("profession", StringType(), True),
                              StructField("titles_ids", StringType(), True)
                            ])
        slist.append(s_names)
        sc.addFile(flist[i])
        df = self.spark.read.option('sep','\t') \
            .schema(slist[i]) \
            .option("nullValue", "\\N") \
            .option("header", True) \
            .csv(SparkFiles.get(flist[i][flist[i].rfind('/')+1:]))
        """Converts certain values to Boolean type"""
        if (i == 4) | (i == 5):
            name = 'is_adult'
            if i == 5:
                name = 'is_original_title'
            udf = UserDefinedFunction(lambda x: (x == 1), BooleanType())
            new_df = df.select(*[udf(column).alias(name) if column == name else column for column in df.columns])
            return new_df
        return df

    def getUkrLanguage(self):
        """Method extracts titles of all ukrainian or ukrainian-translated movies\tv-series etc
        Returns:
            PySpark Dataframe with a selected table
        """
        df = self.getDataFrame("title.akas")
        df.createOrReplaceTempView("title_akas")
        result = spark.sql("SELECT title AS Ukrainian_movies "
                           "FROM title_akas "
                           "WHERE region = 'UA' OR language ='uk'")
        return result


    def getBorn19th(self):
        """Method extracts names of all actors who were born in 19th century
        Returns:
            PySpark Dataframe with a selected table
        """
        df = self.getDataFrame("name.basics")
        df.createOrReplaceTempView("name_basics")
        result = self.spark.sql("SELECT name AS 19th_cent_born "
                           "FROM name_basics "
                           "WHERE birth_year > 1800 AND birth_year < 1901")
        return result


    def getMovies2h(self):
        """Method extracts titles of all movies that last for two hours and more
        Returns:
            PySpark Dataframe with a selected table
        """
        df = self.getDataFrame("title.basics")
        df.createOrReplaceTempView("title_basics")
        result = self.spark.sql("SELECT primary_title AS 2h_movies "
                           "FROM title_basics "
                           "WHERE title_type = 'movie' AND runtime_minutes > 119")
        return result


    def getActors(self):
        """Method extracts names of all actors, titles of movies/tv-series and roles they played 
        in those works
        Returns:
            PySpark Dataframe with a selected table
        """
        df = self.getDataFrame("name.basics")
        df.createOrReplaceTempView("name_basics")
        df = self.getDataFrame("title.basics")
        df.createOrReplaceTempView("title_basics")
        df = self.getDataFrame("title.principals")
        df.createOrReplaceTempView("title_principals")
        result = self.spark.sql("SELECT name_basics.name AS Actor_name, "
                           "title_basics.primary_title AS title, "
                           "title_principals.characters AS Roles "
                           "FROM name_basics "
                           "JOIN title_principals "
                           "ON name_basics.name_id = title_principals.name_id "
                           "JOIN title_basics "
                           "ON title_basics.title_id = title_principals.title_id "
                           "WHERE title_principals.characters IS NOT NULL")
        name = 'characters'
        udf = UserDefinedFunction(lambda x: x[2:-2], StringType())
        new_df = result.select(*[udf(column).alias(name) if column == name else column for column in result.columns])

        return new_df

    def getAdultMoviesPerRegion(self):
        """Method extracts names of 100 titles of movies/tv-series per each region
        Note: there are only 96 regions
        Returns:
            PySpark Dataframe with a selected table
        """
        df = self.getDataFrame("title.basics")
        df.createOrReplaceTempView("title_basics")
        df = self.getDataFrame("title.akas")
        df.createOrReplaceTempView("title_akas")
        result = self.spark.sql("SELECT title_akas.region, COUNT (title_basics.primary_title) "
                           "AS number_of_adult_movies "
                           "FROM title_basics "
                           "JOIN title_akas "
                           "ON title_basics.title_id = title_akas.title_id "
                           "WHERE is_adult AND region IS NOT null "
                           "GROUP BY region "
                           "ORDER BY number_of_adult_movies DESC "
                           "LIMIT 100"
                           )
        return result

    def getEpisodes(self):
        """Method extracts 50 titles of tv-series and number of episodes in each one
        Returns:
            PySpark Dataframe with a selected table
        """
        df = self.getDataFrame("title.basics")
        df.createOrReplaceTempView("title_basics")
        df = self.getDataFrame("title.episode")
        df.createOrReplaceTempView("title_episode")
        result = self.spark.sql("SELECT title_basics.primary_title AS title, t2.number_of_episodes "
                           "FROM title_basics "
                           "JOIN "
                           "(SELECT series_id, COUNT (episode_id) "
                           "AS number_of_episodes "
                           "FROM title_episode "
                           "GROUP BY series_id) "
                           "AS t2 "
                           "ON title_basics.title_id = t2.series_id "
                           "ORDER BY number_of_episodes DESC LIMIT 50"
                           )
        return result

    def getPopularSeriesByD(self):
        """Method extracts titles of 10 the most popular movies/tv-series etc per decade
        Returns:
            PySpark Dataframe with a selected table
        """
        df = self.getDataFrame("title.rating")
        df.createOrReplaceTempView("title_rating")
        df = self.getDataFrame("title.basics")
        df.createOrReplaceTempView("title_basics")
        res0 = self.spark.sql("SELECT MAX(start_year), MIN(start_year) "
                         "FROM title_basics ")
        max = res0.collect()[0][0]//10
        min = res0.collect()[0][1]//10
        schema = StructType([StructField("Year", ShortType(), True),
                              StructField("TV-series", StringType(), True),
                             StructField("Number_of_votes", IntegerType(), True),
                            ])
        result = self.spark.createDataFrame(self.spark.sparkContext.emptyRDD(), schema)
        for i in range(min, max):
            query = "SELECT title_basics.start_year, title_basics.primary_title, title_rating.votes "\
                    "FROM title_basics "\
                    "JOIN title_rating "\
                    "ON title_basics.title_id = title_rating.title_id "\
                    "WHERE title_basics.start_year BETWEEN " + str(i * 10 +1) + " and " + str(i * 10 +10) +" "\
                    "ORDER BY title_rating.votes DESC LIMIT 10"
            result = result.union(self.spark.sql(query))
        return result

    def getPopularSeriesByG(self):
        """Method extracts titles of 10 the most popular movies/tv-series etc per genre
        Returns:
            PySpark Dataframe with a selected table
        """
        df = self.getDataFrame("title.rating")
        df.createOrReplaceTempView("title_rating")
        df = self.getDataFrame("title.basics")
        df.createOrReplaceTempView("title_basics")
        res0 = self.spark.sql("SELECT genres "
                         "FROM title_basics ")
        l = self.spark.sql("SELECT COUNT(genres) FROM title_basics ").collect()[0][0]
        genres = set()
        arr = res0.collect()
        for i in range(l):
            s = arr[i][0]
            if s is not None:
                for w in s.split(","):
                    genres.add(w)
        schema = StructType([StructField("Genre", StringType(), True),
                             StructField("TV-series", StringType(), True),
                             StructField("Number_of_votes", IntegerType(), True),
                            ])
        result = self.spark.createDataFrame(self.spark.sparkContext.emptyRDD(), schema)
        for genre in genres:
            query = "SELECT '"+genre+"', title_basics.primary_title, title_rating.votes "\
                    "FROM title_basics "\
                    "JOIN title_rating "\
                    "ON title_basics.title_id = title_rating.title_id "\
                    "WHERE INSTR('"+genre+"', title_basics.genres) != 0 " \
                    "ORDER BY title_rating.votes DESC LIMIT 10"
            result = result.union(self.spark.sql(query))
        return result

    def write_csv(self, func, filename):
        """Method writes a dataframe to csv file
        Args:
            func(method): method returned Dataframe of which should be written to csv file
            filename(str): name of folder where csv file will be stored
        Returns:
            None
        """
        df = func()
        df.repartition(1).write \
                            .option("header", "true")\
                            .option("sep", "\t")\
                            .csv(filename)


if __name__ == '__main__':
    """Calling functions"""
    '''
    write_csv(getUkrLanguage,'ukr_language_movies')
    write_csv(getBorn19th,'19th_century_born')
    write_csv(getMovies2h,'2h_movies')
    write_csv(getAdultMoviesPerRegion,'adult_movies_per_pegion')
    write_csv(getEpisodes,'number_of_episodes_in_tvseries')
    write_csv(getActors, 'actors')
    write_csv(getPopularSeriesByD,'popular_series_by_decade')
    write_csv(getPopularSeriesByG,'popular_series_by_genre')
    '''
    '''
    getUkrLanguage().show()
    getBorn19th().show()
    getMovies2h().show()
    getActors().show()
    getAdultMoviesPerRegion().show()
    getEpisodes().show()
    getPopularSeriesByD().show()
    getPopularSeriesByG().show()
    '''




